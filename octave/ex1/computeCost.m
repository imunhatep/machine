function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.
%S = 0;
%for i=1:m
%  h = theta(1) + theta(2) * X(i,2);
%  S = S + (h - y(i)) ** 2; 
%end

h = (X * theta);
S = sum((h - y) .^ 2);

J = 1/(2 * m) * S;
% =========================================================================


end
